import './module/sw-cms/block/commerce/similar-products';
import deDE from './module/sw-cms/snippet/de-DE.json';
import enGB from './module/sw-cms/snippet/en-GB.json'

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);