import template from './sw-cms-preview-similar-products.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-preview-similar-products', {
    template
});
