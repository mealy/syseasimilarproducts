import template from './sw-cms-block-similar-products.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-similar-products', {
    template
});
