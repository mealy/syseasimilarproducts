import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'similar-products',
    label: 'sw-cms.blocks.commerce.similarProducts.label',
    category: 'commerce',
    component: 'sw-cms-block-similar-products',
    previewComponent: 'sw-cms-preview-similar-products',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        'emptySlot': {
            type: 'text',
            default: {
                config: {
                    content: {
                        source: 'static',
                        value: ''
                    }
                }
            }
        }
    }
});
