<?php declare(strict_types=1);

namespace SyseaSimilarProducts\Subscriber;

use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\Product\ProductPageCriteriaEvent;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductSubscriber implements EventSubscriberInterface
{
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var EntityRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        EntityRepositoryInterface $categoryRepository,
        SalesChannelRepositoryInterface $productRepository,
        SystemConfigService $systemConfigService
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->systemConfigService = $systemConfigService;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            ProductPageCriteriaEvent::class => 'onProductPageCriteria',
            ProductPageLoadedEvent::class => 'onProductPageLoaded'
        ];
    }

    public function onProductPageCriteria(ProductPageCriteriaEvent $event) {
        $event->getCriteria()->addAssociation('categories');
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event) {
        $page = $event->getPage();
        $product = $page->getProduct();
        $currentProductId = $product->getId();
        $salesChannelContext = $event->getSalesChannelContext();
        $categoryIds = $this->getProductCategories($product);
        if(!empty($categoryIds)) {
            $similarProducts = $this->getSimilarProducts($categoryIds, $currentProductId, $salesChannelContext);
            if(!empty($similarProducts)) {
                $apperanceStyle = 'block';
                $apperance = $this->systemConfigService->get('SyseaSimilarProducts.config.apperanceStyle');
                if($apperance) {
                    $apperanceStyle = 'slider';
                }
                $similarProductsConfig = [
                    'similarProducts' => $similarProducts,
                    'apperance' => $apperanceStyle
                ];
                $similarProductsConfig['boxLayout'] = $this->systemConfigService->get('SyseaSimilarProducts.config.boxLayout');
                $similarProductsConfig['displayMode'] = $this->systemConfigService->get('SyseaSimilarProducts.config.displayMode');
                $similarProductsConfig['container'] = $this->systemConfigService->get('SyseaSimilarProducts.config.container');
                if($apperanceStyle == "slider") {
                    $similarProductsConfig['navigation'] = $this->systemConfigService->get('SyseaSimilarProducts.config.navigation');
                    $similarProductsConfig['elMinWidth'] = $this->systemConfigService->get('SyseaSimilarProducts.config.elMinWidth');
                }

                $page->assign([
                    'similarProductsConfig' =>  $similarProductsConfig
                ]);
            }
        }
    }

    /**
     * @param ProductEntity $product
     * @return array
     */
    private function getProductCategories($product): array {
        $categoryIds = [];
        $categoryLevels = [];
        $categories = $product->getCategories();
        if(empty($categories->getIds())) {
            return [];
        }
        /**
         * @var CategoryEntity $category
         */
        foreach($categories as $category) {
            if(!in_array($category->getLevel(), $categoryLevels)) {
                $categoryLevels[] = $category->getLevel();
            }
        }
        $higehstLevel = max($categoryLevels);
        $tolerance = (int)$this->systemConfigService->get('SyseaSimilarProducts.config.categoryLevelTolerance');
        if($tolerance < 0) {
            $tolerance = 0;
        }
        foreach($categories as $category) {
            if(!empty($category->getId())) {
                if($category->getLevel() == $higehstLevel || $category->getLevel() >= ($higehstLevel - $tolerance)) {
                    if($category->getActive()) {
                        $categoryIds[] = $category->getId();
                    }
                }
            }
            if($this->systemConfigService->get('SyseaSimilarProducts.config.includeParentCategory')) {
                if(!empty($category->getParentId())) {
                    if($category->getLevel() == $higehstLevel || $category->getLevel() >= ($higehstLevel - $tolerance)) {
                        if(!empty($category->getParent())) {
                            $parentCategory = $category->getParent();
                            if($parentCategory->getActive()) {
                                $categoryIds[] = $category->getParentId();
                            }
                        }
                    }
                }
            }
        }
        return $categoryIds;
    }

    private function getSimilarProducts($categoryIds, $currentProductId, $context): ProductCollection {
        if($this->systemConfigService->get('SyseaSimilarProducts.config.randomSelectProducts')) {
            shuffle($categoryIds);
        }
        $similarMax = (int)$this->systemConfigService->get('SyseaSimilarProducts.config.similarProductsLimit');
        if($similarMax > 20) {
            $similarMax = 20; // capped to 20 items to prevent memory limit exceed
        }
        $criteria = new Criteria();
        $criteria->addAssociation('categories');
        $criteria->addAssociations([
            'categories', 'cover', 'manufacturer', 'prices'
        ]);
        $criteria->addFilter(new NotFilter(
            NotFilter::CONNECTION_AND,
            [
                new EqualsFilter('id', $currentProductId)
            ]
        ));
        $criteria->addFilter(new EqualsAnyFilter('categories.id', $categoryIds));
        if(!$this->systemConfigService->get('SyseaSimilarProducts.config.randomSelectProducts')) {
            $criteria->setLimit($similarMax);
        } else {
            $criteria->setLimit(20);
        }
        if($criteria->getLimit() > 20 || $criteria->getLimit() === null) {
            $criteria->setLimit(20);
        }
        $criteria->addFilter(new EqualsFilter('active', true));
        if($this->systemConfigService->get('SyseaSimilarProducts.config.hideSoldoutProducts')) {
            $criteria->addFilter(
                new RangeFilter('stock', [
                    RangeFilter::GT => 0
                ])
            );
        }
        $productSearchResult = $this->productRepository->search($criteria, $context);

        $products = $productSearchResult->getEntities();

        $productIds = [];
        /**
         * @var SalesChannelProductEntity $product
         */
        foreach($products as $product) {
            if(!is_null($product->getConfiguratorGroupConfig()))  {
                $configuratorGroupConfig = $product->getConfiguratorGroupConfig();
                foreach($configuratorGroupConfig as $groupConfig) {
                    if($groupConfig['expressionForListings']) {
                        $productIds[] = $product->getId();
                    } else {
                        if(!is_null($product->getParentId())) {
                            $productIds[] = $product->getParentId();
                        } else {
                            $productIds[] = $product->getId();
                        }
                    }
                }
            } else {
                if(!is_null($product->getParentId())) {
                    $productIds[] = $product->getParentId();
                } else {
                    $productIds[] = $product->getId();
                }
            }
        }

        $criteria->addFilter(new EqualsAnyFilter('id', $productIds));
        $productSearchResult = $this->productRepository->search($criteria, $context);

        if($this->systemConfigService->get('SyseaSimilarProducts.config.randomSelectProducts')) {
            $i = 0;
            $randomProductCollection = new ProductCollection();
            /**
             * @var ProductCollection $productCollection
             */
            $productCollection = $productSearchResult->getEntities();
            $productElements = $productCollection->getElements();
            shuffle($productElements);
            foreach($productElements as $productElement) {
                $randomProductCollection->add($productElement);
                $i++;
                if($i == $similarMax)  {
                    break;
                }
            }
            return $randomProductCollection;
        } else {
            /**
             * @var ProductCollection $similarProducts
             */
            $similarProducts = $productSearchResult->getEntities();
        }
        return $similarProducts;
    }
}